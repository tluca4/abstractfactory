
public class ItalianFactory extends AbstractFactorySemiNumeri {

	@Override
	public Numeri createNumeri() {
		return new NumeriItaliani();
	}

	@Override
	public Semi createSemi() {
		return new SemiItaliani();
	}

}
