
public class MazzoUnicode extends Mazzo {

	AbstractFactorySemiNumeri factory;
	
	public MazzoUnicode() {
		factory = new UnicodeFactory();
	}
	
	@Override
	protected Semi createSemi() {
		return factory.createSemi();
	}

	@Override
	protected Numeri createNumeri() {
		return factory.createNumeri();
	}

}
