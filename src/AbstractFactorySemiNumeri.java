
public abstract class AbstractFactorySemiNumeri {
	public abstract Numeri createNumeri();
	public abstract Semi createSemi();
}
