import java.util.Random;


public abstract class Mazzo {
	
	Semi semi;
	Numeri numeri;
	
	public void initSemi(){
		semi = createSemi();
	}
	
	public void initNumeri(){
		numeri = createNumeri();
	}
	
	protected String generaCard(){
		if(semi == null || numeri == null)
			return "Can't generate card";
		
		String [] s = semi.semi();
		String [] n = numeri.numeri();
		
		Random rand = new Random();
		
		return n[rand.nextInt(10)] + " - " +  s[rand.nextInt(4)];
	}

	protected abstract Semi createSemi();
	protected abstract Numeri createNumeri();
}
