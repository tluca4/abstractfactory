
public class UnicodeFactory extends AbstractFactorySemiNumeri {

	@Override
	public Numeri createNumeri() {
		return new NumeriUnicode();
	}

	@Override
	public Semi createSemi() {
		return new SemiUnicode();
	}

}
