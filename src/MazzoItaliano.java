
public class MazzoItaliano extends Mazzo {
	
	AbstractFactorySemiNumeri factory;
	public MazzoItaliano() {
		factory = new ItalianFactory();
	}
	
	@Override
	protected Semi createSemi() {
		return factory.createSemi();
	}

	@Override
	protected Numeri createNumeri() {
		return factory.createNumeri();
	}

}
